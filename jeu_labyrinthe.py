"""Permet de jouer au jeu du labyrinthe"""
import os
import plateau
import getch
import matrice
import matrice_graphique

def affiche_menu1():
    """Affiche le premier menu en sortie standart"""
    print(' ===== MON SUPER JEU =====')
    print(' (J)ouer ?')
    print(' (Q)uitter ?')


def affiche_menu2():
    """Affiche le deuxième menu (pour jouer) sur la sortie standart"""
    print(' ===== MON SUPER JEU =====')
    print('Choisissez une direction')
    print(plateau.NORD+":NORD\n "+
          plateau.EST+":EST\n "+
          plateau.SUD+":SUD\n "+
          plateau.OUEST+":OUEST\n")


def affiche_jeu(le_plateau, affichage_graphique=None):
    """Permet à l'utilisateur d'interagir avec le jeu
    Sur la sortie standart :
        - le terminal est clear
        - on affiche le deuxième menu
        - on affiche le plateau de jeu
    En mode graphique (si affiche_graphique n'est pas à None)
        - on met à jour l'affichage de la matrice dans une fenêtre pygame
    """
    os.system('clear')
    affiche_menu2()
    matrice.affiche(le_plateau)
    if affichage_graphique is not None:
        affichage_graphique.affiche_matrice()


def saisie_un_seul_caractere():
    """
    Attend que l'utilisateur tape un caractère au clavier et
    renvoie ce caractère sans avoir besoin d'appuyer sur la touche Entrée
    """
    return getch.getch()


def lance_menu():
    """
    Permet à l'utilisateur d'interagir avec le premier menu :
    lancer le jeu ou quitter l'application
    """
    affiche_menu1()
    quitte = False
    while not quitte:
        caractere = saisie_un_seul_caractere()
        if caractere.upper() == 'Q':
            quitte = True
        elif caractere.upper() == 'J':
            os.system('clear')
            affiche_menu_principal()


def affiche_message(message, affichage_graphique=None):
    """Affiche un message
    Sur la sortie standart :
        -  affiche le message sur la sortie standart
    En mode graphique (si affiche_graphique n'est pas à None)
        - affiche le message dans la fenêtre pygame
    """
    print(message)
    if affichage_graphique is not None:
        affichage_graphique.affiche_message(message)


def fin_du_jeu(gagne, affichage_graphique=None):
    """Gère les affichage en fin de partie"""
    if gagne:
        message = 'Bravo ! vous avez gagné !'
    else:
        message = 'Oh non ! Le fantome vous a attrapé !?'
    affiche_message(message, affichage_graphique)
    lance_menu()


def joue(nom_fichier):
    """Permet de lancer le jeu du labyrinthe et y jouer"""
    mon_plateau = plateau.init(nom_fichier)
    if plateau.fabrique_chemin(mon_plateau, (plateau.API_matrice.get_nb_lignes(mon_plateau)-1, plateau.API_matrice.get_nb_colonnes(mon_plateau)-1), (0,0)) == []:
        print("ce labyrinthe est interminable!")
        return
    personnage = (0, 0)
    fantome = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    sortie = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    affichage_graphique = matrice_graphique.MatriceGraphique(mon_plateau)
    affiche_jeu(mon_plateau, affichage_graphique)
    quitte = False
    while not quitte:
        direction = saisie_un_seul_caractere()
        personnage = plateau.deplace_personnage(mon_plateau, personnage, direction)
        fantome = plateau.deplace_fantome(mon_plateau, fantome, personnage)
        affiche_jeu(mon_plateau, affichage_graphique)
        if personnage == fantome:
            fin_du_jeu(False, affichage_graphique)
            quitte = True
        elif personnage == sortie:
            fin_du_jeu(True, affichage_graphique)
            quitte = True
    return True

def Flash(nom_fichier):
    mon_plateau = plateau.init(nom_fichier)
    if plateau.fabrique_chemin(mon_plateau, (plateau.API_matrice.get_nb_lignes(mon_plateau)-1, plateau.API_matrice.get_nb_colonnes(mon_plateau)-1), (0,0)) == []:
        print("ce labyrinthe est interminable!")
        return False
    personnage = (0, 0)
    fantome = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    sortie = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    affichage_graphique = matrice_graphique.MatriceGraphique(mon_plateau)
    affiche_jeu(mon_plateau, affichage_graphique)
    quitte = False
    while not quitte:
        direction = saisie_un_seul_caractere()
        personnage = plateau.deplace_personnage(mon_plateau, personnage, direction)
        fantome = plateau.deplace_fantome(mon_plateau, plateau.deplace_fantome(mon_plateau, fantome, personnage), personnage)
        affiche_jeu(mon_plateau, affichage_graphique)
        if personnage == fantome:
            fin_du_jeu(False, affichage_graphique)
            quitte = True
        elif personnage == sortie:
            fin_du_jeu(True, affichage_graphique)
            quitte = True
    return True

def Expert(nom_fichier):
    mon_plateau = plateau.init(nom_fichier)
    if plateau.fabrique_chemin(mon_plateau, (plateau.API_matrice.get_nb_lignes(mon_plateau)-1, plateau.API_matrice.get_nb_colonnes(mon_plateau)-1), (0,0)) == []:
        print("ce labyrinthe est interminable!")
        return False
    personnage = (0, 0)
    fantome1 = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    fantome2 = plateau.generer_case_deuxieme_fantome(mon_plateau, fantome1)
    plateau.API_matrice.set_val(mon_plateau,fantome2[0],fantome2[1],plateau.FANTOME)
    sortie = (matrice.get_nb_lignes(mon_plateau) - 1, matrice.get_nb_colonnes(mon_plateau) - 1)
    affichage_graphique = matrice_graphique.MatriceGraphique(mon_plateau)
    affiche_jeu(mon_plateau, affichage_graphique)
    quitte = False
    while not quitte:
        direction = saisie_un_seul_caractere()
        personnage = plateau.deplace_personnage(mon_plateau, personnage, direction)
        fantome1, fantome2 = plateau.deplace_deux_fantomes(mon_plateau, fantome1, fantome2, personnage)
        affiche_jeu(mon_plateau, affichage_graphique)
        if personnage == fantome1 or personnage == fantome2:
            fin_du_jeu(False, affichage_graphique)
            quitte = True
        elif personnage == sortie:
            fin_du_jeu(True, affichage_graphique)
            quitte = True
    return True

""" chemin = input("Entrez un chemin vers un labyrinthe: ")
while not joue(chemin):
    chemin = input("Entrez un chemin vers un labyrinthe: ") """
    
def demander_choix(deb, fin):
    """demande à l'utilisateur d'entrer un choix pour une réponse entre la réponse deb et la réponse fin

    Args:
        deb (int): numéro de la première question
        fin (int): numéro de la dernière question

    Returns:
        int: la réponse choisie
    """    
    choix = 0
    while not choix in range(deb, fin+1):
        choix = saisie_un_seul_caractere()
        try:
            choix = int(choix)
        except ValueError:
            pass
    return choix
    
def affiche_difficultes():
    print(' ===== MON SUPER JEU =====')
    print("1. Standard")
    print("2. Flash")
    print("3. Expert")
    choix = demander_choix(1,3)
    os.system("clear")
    return choix
    
def affiche_menu_principal(difficulte=1, labyrinthe="labyrinthe1.txt"):
    # affiche un menu qui nous permet de choisir la difficulté du jeu et le labyrinthe
    print(' ===== MON SUPER JEU =====')
    difficulte_affiche = ""
    if difficulte == 1:
        difficulte_affiche = "Standard"
    elif difficulte == 2:
        difficulte_affiche = "Flash"
    else:
        difficulte_affiche = "Expert"
    print("1. Choisir une difficulté | ", difficulte_affiche)
    print("2. Choisir un labyrinthe  | ", labyrinthe)
    print("3. Lancer la partie")
    choix = demander_choix(1,3)
    if choix == 1:
        os.system("clear")
        difficulte = affiche_difficultes()
        affiche_menu_principal(difficulte, labyrinthe)
    elif choix == 2:
        labyrinthe_valide = False
        while not labyrinthe_valide:
            labyrinthe = input("Veuillez entrer le chemin vers le labyrinthe: ")
            try:
                open(labyrinthe, "r")
                labyrinthe_valide = True
            except:
                print("ce labyrinthe n'existe pas")
        os.system("clear")
        affiche_menu_principal(difficulte, labyrinthe)
    else:
        os.system("clear")
        if difficulte == 1:
            if not joue(labyrinthe):
                affiche_menu_principal(difficulte)
        elif difficulte == 2:
            if not Flash(labyrinthe):
                affiche_menu_principal(difficulte)
        else:
            if not Expert(labyrinthe):
                affiche_menu_principal(difficulte)
        
    
affiche_menu_principal()