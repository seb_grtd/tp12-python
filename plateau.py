"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice as API_matrice
import copy

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 'w'
EST = 's'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    matrice = API_matrice.charge_matrice(nom_fichier)
    API_matrice.set_val(matrice,0,0,PERSONNAGE)
    API_matrice.set_val(matrice,-1,-1,FANTOME)
    return matrice

def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    return position[0] in range(API_matrice.get_nb_lignes(le_plateau)) and position[1] in range(API_matrice.get_nb_colonnes(le_plateau))

def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    if est_sur_le_plateau(le_plateau, position):
        return API_matrice.get_val(le_plateau, position[0], position[1])


def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    return get(le_plateau, position) == MUR


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    return get(le_plateau, position) == FANTOME

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    return position[0] == API_matrice.get_nb_lignes(le_plateau)-1 and position[1] == API_matrice.get_nb_colonnes(le_plateau)-1


def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plateau ni traverser les murs
       Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    if direction == SUD and not personnage[0] >= API_matrice.get_nb_lignes(le_plateau)-1 and not get(le_plateau, (personnage[0]+1, personnage[1])) == MUR:
        API_matrice.set_val(le_plateau, personnage[0], personnage[1], COULOIR)
        API_matrice.set_val(le_plateau, personnage[0]+1, personnage[1], PERSONNAGE)
        return personnage[0]+1, personnage[1]
    elif direction == NORD and not personnage[0] <= 0 and not get(le_plateau, (personnage[0]-1, personnage[1])) == MUR:
        API_matrice.set_val(le_plateau, personnage[0], personnage[1], COULOIR)
        API_matrice.set_val(le_plateau, personnage[0]-1, personnage[1], PERSONNAGE)
        return personnage[0]-1, personnage[1]
    elif direction == OUEST and not personnage[1] <= 0 and not get(le_plateau, (personnage[0], personnage[1]-1)) == MUR:
        API_matrice.set_val(le_plateau, personnage[0], personnage[1], COULOIR)
        API_matrice.set_val(le_plateau, personnage[0], personnage[1]-1, PERSONNAGE)
        return personnage[0], personnage[1]-1
    elif direction == EST and not personnage[1] >= API_matrice.get_nb_colonnes(le_plateau)-1 and not get(le_plateau, (personnage[0], personnage[1]+1)) == MUR:
        API_matrice.set_val(le_plateau, personnage[0], personnage[1], COULOIR)
        API_matrice.set_val(le_plateau, personnage[0], personnage[1]+1, PERSONNAGE)
        return personnage[0], personnage[1]+1
    return personnage     


def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    ensemble_pos = set()
    if not position[0] >= API_matrice.get_nb_lignes(le_plateau)-1 and not get(le_plateau, (position[0]+1, position[1])) == MUR:
        ensemble_pos.add((position[0]+1, position[1]))
    if not position[0] <= 0 and not get(le_plateau, (position[0]-1, position[1])) == MUR:
        ensemble_pos.add((position[0]-1, position[1]))
    if not position[1] >= API_matrice.get_nb_colonnes(le_plateau)-1 and not get(le_plateau, (position[0], position[1]+1)) == MUR:
        ensemble_pos.add((position[0], position[1]+1))
    if not position[1] <= 0 and not get(le_plateau, (position[0], position[1]-1)) == MUR:
        ensemble_pos.add((position[0], position[1]-1))
    return ensemble_pos

def voisins_non_none(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un None
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    ensemble_pos = set()
    if not position[0] > API_matrice.get_nb_lignes(le_plateau) and not get(le_plateau, (position[0]+1, position[1])) == None and est_sur_le_plateau(le_plateau,(position[0]+1, position[1])):
        ensemble_pos.add((position[0]+1, position[1]))
    if not position[0] == 0 and not get(le_plateau, (position[0]-1, position[1])) == None and est_sur_le_plateau(le_plateau, (position[0]-1, position[1])):
        ensemble_pos.add((position[0]-1, position[1]))
    if not position[1] > API_matrice.get_nb_colonnes(le_plateau) and not get(le_plateau, (position[0], position[1]+1)) == None and est_sur_le_plateau(le_plateau, (position[0], position[1]+1)):
        ensemble_pos.add((position[0], position[1]+1))
    if not position[1] == 0 and not get(le_plateau, (position[0], position[1]-1)) == None and est_sur_le_plateau(le_plateau , (position[0], position[1]-1)):
        ensemble_pos.add((position[0], position[1]-1))
    return ensemble_pos

def changer_murs_en_None_et_vider_plateau(le_plateau):
    """change les murs (1) du labyrinthe en None et transforme les cases où il y a un fantôme ou un personnage en une case COULOIR

    Args:
        le_plateau (plateau): un plateau de jeu
    """    
    for i in range(API_matrice.get_nb_lignes(le_plateau)):
        for j in range(API_matrice.get_nb_colonnes(le_plateau)):
            if le_plateau[i][j] == MUR:
                le_plateau[i][j] = None
            if le_plateau[i][j] == PERSONNAGE or le_plateau[i][j] == FANTOME:
                le_plateau[i][j] = COULOIR

def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    nouv_plateau = copy.deepcopy(le_plateau)
    changer_murs_en_None_et_vider_plateau(nouv_plateau)
    # On fait une copie du plateau d'origine où l'on change chaque mur(1) en None
    # et on change les cases contenant un fantôme ou le personnage pour des couloirs(0)
    a_visiter = [position_depart]
    # Pour bien visiter chaque case on fait une pile des cases à visiter que l'on remplira au fur et à mesure
    while not a_visiter == []:
        case_actuelle = a_visiter.pop()
        for case_voisine in voisins(nouv_plateau, case_actuelle):
            valeur_case_voisine = get(nouv_plateau, case_voisine)
            if not valeur_case_voisine == None and not case_voisine == position_depart: 
                if valeur_case_voisine == 0:
                    # si la case voisine n'est pas un mur ni la case de départ et n'a pas déjà été visité 
                    # on l'ajoute dans notre pile de cases à visiter
                    # on change la valeur de cette case dans la copie du plateau par la valeur de la case_actuelle +1
                    a_visiter.append(case_voisine)
                    API_matrice.set_val(nouv_plateau, case_voisine[0], case_voisine[1], get(nouv_plateau, case_actuelle)+1)
                else: 
                    # si la case voisine n'est ni un mur ni la case de départ mais qu'elle ne vaut pas 0
                    # on en déduit qu'elle a déjà été visitée avant
                    # si la valeur de la longueur du chemin vers la case en partant de cette case est plus petite
                    # que la valeur actuelle, on la remplace.
                    # Finalement on ajoute dans notre pile de cases à visiter cette case (toujous si les conditions précédentes étaient vérifiées)
                    # cela nous permettra de voir si d'autres chemins plus courts existent pour les cases adjacentes de cette cases etc...
                    if valeur_case_voisine > get(nouv_plateau, case_actuelle)+1:
                        API_matrice.set_val(nouv_plateau, case_voisine[0], case_voisine[1], get(nouv_plateau, case_actuelle)+1)
                        a_visiter.append(case_voisine)
    return nouv_plateau

def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    calque = fabrique_le_calque(le_plateau, position_depart)
    position_actuelle = position_arrivee
    # on part de la fin vers le début
    chemin = [position_actuelle]
    if get(calque, position_arrivee) == 0:
        # si la position d'arrivee est impossible à atteindre elle vaut alors 0 dans le calque
        # dans ce cas on retourne une liste vide
        return []
    while not position_actuelle == position_depart:
        for case_voisine in voisins_non_none(calque, position_actuelle):
            if get(calque, case_voisine) == get(calque, position_actuelle) - 1:
                chemin.append(case_voisine)
                position_actuelle = case_voisine
                break
    chemin.pop()
    # on supprime la case de depart de la fin de la liste
    return chemin

def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    plus_court_chemin = fabrique_chemin(le_plateau, fantome, personnage)
    if len(plus_court_chemin) <= 0:
        return fantome
    API_matrice.set_val(le_plateau, fantome[0], fantome[1], COULOIR)
    API_matrice.set_val(le_plateau, plus_court_chemin[-1][0], plus_court_chemin[-1][1], FANTOME)
    return plus_court_chemin[-1][0], plus_court_chemin[-1][1]

# ----------------
# fonctions pour le mode Expert

def generer_case_deuxieme_fantome(le_plateau, fantome):
    """trouve une case voisine à celle du premier fantôme où placer le deuxième fantôme

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau

    Returns:
        [tuple]: la position où placer le fantome 2
    """    
    for voisin in voisins(le_plateau, fantome):
        return voisin

def deplace_deux_fantomes(le_plateau, fantome1, fantome2, personnage):
    """déplace les deux FANTOME sur le plateau (expert) vers le personnage en prenant le chemin le plus court (les fantômes prennent des chemins différents)

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome1 (tuple): la position du fantome1 sur le plateau
        fantome2 (tuple): la position du fantome1 sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        (tuple, tuple): la nouvelle position des deux fantomes (1 et 2)
    """
    plus_court_chemin = fabrique_chemin(le_plateau, fantome1, personnage)
    case_trouvee = False
    # on fait une variable case_trouvee qui nous permettra de savoir plus tard si l'on a trouvé ou non 
    # une case où déplacer le fantôme 2 pour qu'il prenne une direction différente que le fantôme 1 (plutôt que les deux fantômes se collent)
    if fantome2 in voisins(le_plateau, fantome1):
        for voisin_fantome2 in voisins(le_plateau, fantome2):
            if not fantome1 == voisin_fantome2 and not voisin_fantome2 == plus_court_chemin[-1] and not case_trouvee:
                API_matrice.set_val(le_plateau, fantome2[0], fantome2[1], COULOIR)
                API_matrice.set_val(le_plateau, voisin_fantome2[0], voisin_fantome2[1], FANTOME)
                fantome2 = voisin_fantome2
                case_trouvee = True
    fantome1 = deplace_fantome(le_plateau, fantome1, personnage)
    if not case_trouvee:
        fantome2 = deplace_fantome(le_plateau, fantome2, personnage)
    return fantome1, fantome2

